// ต้องใส่ export เพื่อทำให้รู้ว่าต้องส่งออกไป
// export default คือตัวหลักที่ส่งออกไป
export interface User {
  id: number;
  login: string;
  name: string;
  password: string;
}
