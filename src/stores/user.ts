import { ref, computed } from "vue"; //import ธรรมดา
import { defineStore } from "pinia"; //import ธรรมดา
import type { User } from "@/types/User"; //import class

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const editedUser = ref<User>({ id: -1, login: "", name: "", password: "" });
  let lastID = 4;
  // ref เป็นตัวแปรพวก reactive
  // reactive คือคุณลักษณะที่ไปทำให้ตัว UI รู้ว่าตัวแปรตัวนี้มีการเปลี่ยนแปลง
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Adminstrator", password: "Pass@1234" },
    { id: 2, login: "user1", name: "User 1", password: "Pass@1234" },
    { id: 3, login: "user2", name: "User 2", password: "Pass@1234" }, // {} คือการสร้าง Object ขึ้นมาใหม่
  ]); // ถ้าเป็น List จะนิยมใส่ s ข้างหลังตัวแปร
  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if (index >= 0) {
      const user = users.value[index]; //ตัวที่หาเจอเก็บใส่ user
      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };
  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);
    users.value.splice(index, 1);
  };

  const saveUser = () => {
    if (editedUser.value.id < 0) {
      // Add new
      editedUser.value.id = lastID++;
      users.value.push(editedUser.value);
    } else {
      const index = users.value.findIndex(
        (item) => item.id === editedUser.value.id
      );
      users.value[index] = editedUser.value;
    }
    dialog.value = false;
    clear();
  };

  const editUser = (user: User) => {
    // ... ทำให้สิ่งที่อยู่ใน user กระจายตัวออกเป็น objectตัวใหม่
    editedUser.value = { ...user }; // spreed ออกมา
    // JSON.parse(JSON.stringify(user)); จะลึกกว้า ...
    dialog.value = true;
  };

  const clear = () => {
    editedUser.value = { id: -1, login: "", name: "", password: "" };
  };
  return {
    users,
    deleteUser,
    dialog,
    editedUser,
    clear,
    saveUser,
    editUser,
    isTable,
    login,
  };
});
